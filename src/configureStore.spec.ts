import { configureStore } from './configureStore';

describe('configureStore', () => {
    it('returned store\'s state should have exchange rates state', () => {
        expect(configureStore().getState()).toHaveProperty('exchangeRates');
    });
});
