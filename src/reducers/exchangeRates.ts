import { FetchingStateAction, FETCHING_STATE_ACTION } from '../actions/fetchingState';
import { ErrorStateAction, ERROR_STATE_ACTION } from '../actions/errorState';
import { ReadyStateAction, READY_STATE_ACTION } from '../actions/readyState';

export enum AppState {
    Initial = 'INITIAL_STATE',
    Fetching = 'FETCHING_STATE',
    Error = 'ERROR_STATE',
    Ready = 'READY_STATE',
}

export interface ExchangeRate {
    code: string;
    rate: number;
}

export interface ExchangeRatesResults {
    rates: ExchangeRate[];
    date: string;
    base: string;
}

export interface ExchangeRatesState {
    appState: AppState;
    results?: ExchangeRatesResults;
    error?: string;
}

export const initialState: ExchangeRatesState = {
    appState: AppState.Initial,
};

export type ExchangeRatesActions = FetchingStateAction | ErrorStateAction | ReadyStateAction;

export function exchangeRates(state = initialState, action: ExchangeRatesActions) {
    switch (action.type) {
        case FETCHING_STATE_ACTION:
            return {
                ...state,
                appState: AppState.Fetching,
            };

        case ERROR_STATE_ACTION:
            return {
                ...state,
                error: action.error,
                appState: AppState.Error,
            };

        case READY_STATE_ACTION:
            return {
                ...state,
                results: action.results,
                appState: AppState.Ready,
            };

        default:
            return state;
    }
}
