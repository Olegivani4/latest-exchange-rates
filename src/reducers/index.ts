import { combineReducers } from 'redux';
import { exchangeRates, ExchangeRatesState } from './exchangeRates';

export interface State {
    exchangeRates?: ExchangeRatesState;
}

export const rootReducer = combineReducers({
    exchangeRates,
});
