import { exchangeRates, initialState, AppState } from './exchangeRates';
import { FETCHING_STATE_ACTION } from '../actions/fetchingState';
import { ERROR_STATE_ACTION } from '../actions/errorState';
import { READY_STATE_ACTION } from '../actions/readyState';

describe('reducers/exchangeRates', () => {
    it('should change AppState to Fetching when receives FETCHING_STATE_ACTION', () => {
        expect(exchangeRates(initialState, { type: FETCHING_STATE_ACTION })).toEqual({
            ...initialState,
            appState: AppState.Fetching,
        });
    });

    it('should change AppState to Error when receives ERROR_STATE_ACTION', () => {
        expect(exchangeRates(initialState, { type: ERROR_STATE_ACTION, error: 'foo' })).toEqual({
            ...initialState,
            appState: AppState.Error,
            error: 'foo',
        });
    });

    it('should change AppState to Ready when receives READY_STATE_ACTION', () => {
        const results = {
            base: 'EUR',
            date: '1997-01-01',
            rates: [
                {
                    code: 'USD',
                    rate: 1.147,
                },
                {
                    code: 'CAD',
                    rate: 1.4947,
                },
            ],
        }
        expect(exchangeRates(initialState, { results, type: READY_STATE_ACTION })).toEqual({
            ...initialState,
            results,
            appState: AppState.Ready,
        });
    });
});
