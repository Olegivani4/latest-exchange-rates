import { createStore, Store } from 'redux';
import { rootReducer, State } from './reducers';

export function configureStore(): Store<State> {
    return createStore(rootReducer, {});
}
