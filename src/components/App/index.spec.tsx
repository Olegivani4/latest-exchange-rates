import React from 'react';
import { Store } from 'redux';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import { App } from './';
import { ExchangeRatesComponent, loadingMessage } from '../ExchangeRates';
import { configureStore } from '../../configureStore';
import { State } from '../../reducers';

describe('components/App', () => {
    let store: Store<State>;

    beforeEach(() => {
        store = configureStore();
    });

    it('should render ExchangeRatesComponent component', () => {
        const subject = mount(
            <Provider store={store}>
                <App />
            </Provider>);

        expect(subject.find(ExchangeRatesComponent)).toExist();
        subject.unmount();
    });

    // Integration tests

    it('should show loadingMessage message while exchange rates results are fetching', () => {
        (window as any).fetch = () => new Promise(resolve => window.setTimeout(resolve, 10))
        const subject = mount(
            <Provider store={store}>
                <App />
            </Provider>);

        subject.find('button').simulate('click');
        expect(subject.find('span')).toHaveText(loadingMessage);
        subject.unmount();
    });

    it('should show result table when exchange rates results are fetched', async () => {
        expect.assertions(2);
        (window as any).fetch = () => Promise.resolve({
            ok: true,
            json: () => ({
                date: '1997-01-01',
                base: 'EUR',
                rates: {
                    USD: 1.147,
                    CAD: 1.4947,
                },
            }),
        });
        const subject = mount(
            <Provider store={store}>
                <App />
            </Provider>);

        subject.find('button').simulate('click');
        await new Promise(resolve => window.setTimeout(resolve));
        subject.update();

        expect(subject.find('tbody tr')).toHaveLength(2);
        expect(subject.find('.alert')).not.toExist();
        subject.unmount();
    });

    it('should show an error message if fetching failed', async () => {
        expect.assertions(2);
        (window as any).fetch = () => Promise.resolve({
            ok: false,
            status: 404,
            statusText: 'Page not found',
        });
        const subject = mount(
            <Provider store={store}>
                <App />
            </Provider>);

        subject.find('button').simulate('click');
        await new Promise(resolve => window.setTimeout(resolve));
        subject.update();

        expect(subject.find('tbody tr')).toHaveLength(0);
        expect(subject.find('.alert')).toExist();
        subject.unmount();
    });
});
