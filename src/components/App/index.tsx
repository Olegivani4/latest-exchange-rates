import { connect } from 'react-redux';
import { fetchingState } from '../../actions/fetchingState';
import { errorState } from '../../actions/errorState';
import { readyState } from '../../actions/readyState';
import { State } from '../../reducers';
import { ExchangeRatesComponent } from '../ExchangeRates';

export interface AppDispatchProps {
    fetchingState: typeof fetchingState;
    errorState: typeof errorState;
    readyState: typeof readyState;
}

const mapStateToProps = (state: State) => state.exchangeRates;
const mapDispatchToProps: AppDispatchProps = { fetchingState, errorState, readyState };

export const App = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ExchangeRatesComponent);
