import React from 'react';
import { shallow } from 'enzyme';
import { ResultsTable } from './';
import { ExchangeRatesResults } from '../../../reducers/exchangeRates';

const results: ExchangeRatesResults = {
    date: '1997-01-01',
    base: 'EUR',
    rates: [
        {
            code: 'USD',
            rate: 1.147,
        },
        {
            code: 'CAD',
            rate: 1.4947,
        },
    ],
};

describe('components/ExchangeRates/ResultTable', () => {
    it('should display date of exchange rates', () => {
        const subject = shallow(<ResultsTable {...results} />);
        expect(subject.find('#date span')).toHaveText(results.date);
    });

    it('should display base of exchange rates', () => {
        const subject = shallow(<ResultsTable {...results} />);
        expect(subject.find('#base span')).toHaveText(results.base);
    });

    it('should render all exchange rates in table', () => {
        const subject = shallow(<ResultsTable {...results} />);
        expect(subject.find('tbody tr')).toHaveLength(results.rates.length);
    });
});
