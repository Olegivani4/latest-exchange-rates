import React from 'react';
import { ExchangeRatesResults } from '../../../reducers/exchangeRates';

export const ResultsTable = ({ date, rates, base }: ExchangeRatesResults) => {
    const rows = !rates ? null : rates.map((row) => {
        return (
            <tr key={row.code}>
                <td>{row.code}</td>
                <td>{row.rate}</td>
            </tr>
        );
    });

    return (
        <div>
            <div id="date">Date: <span>{date}</span></div>
            <div id="base">Base: <span>{base}</span></div>

            <table className="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">Currency</th>
                        <th scope="col">Rate</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        </div>
    );
};
