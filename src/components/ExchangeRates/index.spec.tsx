import React from 'react';
import { shallow } from 'enzyme';
import { ExchangeRatesComponent, loadingMessage } from './';
import { AppState } from '../../reducers/exchangeRates';
import { fetchingState } from '../../actions/fetchingState';
import { errorState } from '../../actions/errorState';
import { readyState } from '../../actions/readyState';
import { FetchButton } from './FetchButton';
import { ErrorMessage } from './ErrorMessage';
import { ResultsTable } from './ResultsTable';

const dispatchers = {
    fetchingState,
    errorState: jest.fn().mockImplementation(errorState),
    readyState: jest.fn().mockImplementation(readyState),
};

describe('components/ExchangeRates', () => {
    it('should render FetchButton component if AppState equals to Initial', () => {
        const subject = shallow(<ExchangeRatesComponent appState={AppState.Initial} {...dispatchers} />);
        expect(subject.find(FetchButton)).toExist();
    });

    it('should render "Loading" if AppState equals to Fetching', () => {
        (window as any).fetch = () => Promise.reject(new Error('foo'));
        const subject = shallow(<ExchangeRatesComponent appState={AppState.Fetching} {...dispatchers} />);
        expect(subject.find('span')).toHaveText(loadingMessage);
    });

    it('should dispatch error if Fetching failed', async () => {
        (window as any).fetch = () => Promise.reject(new Error('foo'));
        shallow(<ExchangeRatesComponent appState={AppState.Fetching} {...dispatchers} />);
        await new Promise(resolve => window.setTimeout(resolve));
        expect(dispatchers.errorState).toHaveBeenCalledWith('Error: foo');
    });

    it('should dispatch results if Fetching succeed', async () => {
        (window as any).fetch = jest.fn().mockImplementation(() => Promise.resolve({
            ok: true,
            json: () => ({
                date: '1997-01-01',
                base: 'EUR',
                rates: {
                    USD: 1.147,
                },
            }),
        }));
        shallow(<ExchangeRatesComponent appState={AppState.Fetching} {...dispatchers} />);
        await new Promise(resolve => window.setTimeout(resolve));
        expect(dispatchers.readyState).toHaveBeenCalledWith({
            date: '1997-01-01',
            base: 'EUR',
            rates: [
                {
                    code: 'USD',
                    rate: 1.147,
                },
            ],
        });
    });

    it('should render ErrorMessage component if AppState equals to Error', () => {
        const subject = shallow(<ExchangeRatesComponent appState={AppState.Error} {...dispatchers} />);
        expect(subject.find(ErrorMessage)).toExist();
    });

    it('should render ResultsTable component if AppState equals to Ready', () => {
        const subject = shallow(<ExchangeRatesComponent appState={AppState.Ready} {...dispatchers} />);
        expect(subject.find(ResultsTable)).toExist();
    });
});
