import React from 'react';
import { AppState, ExchangeRatesResults, ExchangeRatesState } from '../../reducers/exchangeRates';
import { FetchButton } from './FetchButton';
import { AppDispatchProps } from '../App';
import { ErrorMessage } from './ErrorMessage';
import { fetchExchangeRates } from './fetchExchangeRates';
import { ResultsTable } from './ResultsTable';

export interface ExchangeRatesComponentProps extends ExchangeRatesState, AppDispatchProps {}

export const loadingMessage = 'Loading';

export const ExchangeRatesComponent = (props: ExchangeRatesComponentProps) => {
    switch (props.appState) {
        case AppState.Initial:
            return <FetchButton onClick={props.fetchingState} />;

        case AppState.Fetching:
            fetchExchangeRates()
                .then((results: ExchangeRatesResults) => props.readyState(results))
                .catch(e => props.errorState(`Error: ${e.message}`));

            return (
                <div className="centred">
                    <span>{loadingMessage}</span>
                </div>
            );

        case AppState.Error:
            return <ErrorMessage error={props.error} />;

        case AppState.Ready:
            return <ResultsTable {...props.results} />;
    }
};
