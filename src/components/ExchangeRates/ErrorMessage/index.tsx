import React from 'react';

export const ErrorMessage = ({ error }: {
    error: string;
}) => (
    <div className="alert alert-danger" role="alert">
        {error}
    </div>
);
