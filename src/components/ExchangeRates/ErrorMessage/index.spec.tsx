import React from 'react';
import { shallow } from 'enzyme';
import { ErrorMessage } from './';

describe('components/ExchangeRates/ErrorMessage', () => {
    it('should display error message', () => {
        const subject = shallow(<ErrorMessage error="foo" />);
        expect(subject).toHaveText('foo');
    });
});
