import React from 'react';

export const FetchButton = ({ onClick }: {
    onClick: () => void;
}) => (
    <div className="centred">
        <button
            type="button"
            className="btn btn-primary btn-lg"
            id="fetchButton"
            onClick={onClick}
        >
            Fetch exchange rate results
        </button>
    </div>
);
