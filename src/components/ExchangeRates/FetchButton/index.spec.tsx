import React from 'react';
import { shallow } from 'enzyme';
import { FetchButton } from './';

describe('components/ExchangeRates/FetchButton', () => {
    it('should render fetch button', () => {
        const subject = shallow(<FetchButton onClick={jest.fn()} />);
        expect(subject.find('#fetchButton')).toExist();
    });

    it('should call onClick callback when button was clicked', () => {
        const onClick = jest.fn();
        const subject = shallow(<FetchButton onClick={onClick} />);
        subject.find('#fetchButton').simulate('click');
        expect(onClick).toHaveBeenCalled();
    });
});
