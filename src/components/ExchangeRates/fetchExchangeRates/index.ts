import { ExchangeRatesResults } from '../../../reducers/exchangeRates';

const apiUrl = 'https://api.exchangeratesapi.io/latest';

export function fetchExchangeRates(): Promise<ExchangeRatesResults> {
    return fetch(apiUrl)
        .then((response: Response) => {
            if (response.ok) {
                return response.json();
            }

            throw new Error(`HTTP ${response.status} ${response.statusText}`);
        })
        .then(({ rates, date, base }) => {
            if (!rates || !date || !base) {
                throw new Error('Wrong response json structure');
            }

            const parsedRates = Object.entries(rates)
                .map((row: [string, any]) => ({
                    code: row[0],
                    rate: parseFloat(row[1]),
                }));

            return {
                date,
                base,
                rates: parsedRates,
            };
        });
}
