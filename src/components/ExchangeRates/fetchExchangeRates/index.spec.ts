import { fetchExchangeRates } from './';

describe('components/ExchangeRates/fetchExchangeRates', () => {
    it('it should throw an Error if fetch result is not ok', () => {
        expect.assertions(1);
        (window as any).fetch = jest.fn().mockImplementation(() => Promise.resolve({
            ok: false,
            status: 404,
            statusText: 'Page not found',
        }));
        return fetchExchangeRates().catch((e: Error) => {
            expect(e.message).toBe('HTTP 404 Page not found');
        });
    });

    it('it should throw an Error if fetch fails', () => {
        expect.assertions(1);
        (window as any).fetch = jest.fn().mockImplementation(() => Promise.reject(new Error('foo')));
        return fetchExchangeRates().catch((e: Error) => {
            expect(e.message).toBe('foo');
        });
    });

    it('it should throw an Error if fetch result is ok but json structure is wrong', () => {
        (window as any).fetch = jest.fn().mockImplementation(() => Promise.resolve({
            ok: true,
            json: () => ({}),
        }));
        return fetchExchangeRates().catch((e: Error) => {
            expect(e.message).toBe('Wrong response json structure');
        });
    });

    it('it should call onReady callback if fetch result is ok but json structure is wrong', async () => {
        (window as any).fetch = jest.fn().mockImplementation(() => Promise.resolve({
            ok: true,
            json: () => ({
                date: '1997-01-01',
                base: 'EUR',
                rates: {
                    USD: 1.147,
                },
            }),
        }));
        const results = await fetchExchangeRates();
        expect(results).toEqual({
            date: '1997-01-01',
            base: 'EUR',
            rates: [
                {
                    code: 'USD',
                    rate: 1.147,
                },
            ],
        });
    });
});
