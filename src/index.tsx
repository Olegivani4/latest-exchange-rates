import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './configureStore';
import { App } from './components/App';

import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css';
import 'whatwg-fetch';

const store = configureStore();

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'),
);
