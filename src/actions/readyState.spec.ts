import { readyState, READY_STATE_ACTION } from './readyState';
import { ExchangeRatesResults } from '../reducers/exchangeRates';

describe('actions/readyState', () => {
    it('should return action to ready state', () => {
        const results: ExchangeRatesResults = {
            rates: [
                {
                    code: 'USD',
                    rate: 1.147,
                },
            ],
            date: '1997-01-01',
            base: 'EUR',
        };
        expect(readyState(results)).toEqual({
            results,
            type: READY_STATE_ACTION,
        });
    });
});
