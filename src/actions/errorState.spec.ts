import { errorState, ERROR_STATE_ACTION } from './errorState';

describe('actions/errorState', () => {
    it('should return Error action', () => {
        expect(errorState('some error')).toEqual({
            type: ERROR_STATE_ACTION,
            error: 'some error',
        });
    });
});
