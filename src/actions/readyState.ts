import { ExchangeRatesResults } from '../reducers/exchangeRates';

export const READY_STATE_ACTION = 'READY_STATE_ACTION';

export interface ReadyStateAction {
    type: typeof READY_STATE_ACTION;
    results: ExchangeRatesResults;
}

export function readyState(results: ExchangeRatesResults): ReadyStateAction {
    return {
        results,
        type: READY_STATE_ACTION,
    };
}
