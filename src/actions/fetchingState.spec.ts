import { fetchingState, FETCHING_STATE_ACTION } from './fetchingState';

describe('actions/fetchingState', () => {
    it('should return fetching action', () => {
        expect(fetchingState()).toEqual({
            type: FETCHING_STATE_ACTION,
        });
    });
});
