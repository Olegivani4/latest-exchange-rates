export const FETCHING_STATE_ACTION = 'FETCHING_STATE_ACTION';

export interface FetchingStateAction {
    type: typeof FETCHING_STATE_ACTION;
}

export function fetchingState(): FetchingStateAction {
    return {
        type: FETCHING_STATE_ACTION,
    };
}
