export const ERROR_STATE_ACTION = 'ERROR_STATE_ACTION';

export interface ErrorStateAction {
    type: typeof ERROR_STATE_ACTION;
    error: string;
}

export function errorState(error: string): ErrorStateAction {
    return {
        error,
        type: ERROR_STATE_ACTION,
    };
}
