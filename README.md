# Project requirements

- Initially there should be a button. Data is fetched when the button is clicked
- "Loading" message/UI component should be shown while data is fetched
- Error modal should be shown when data fetch fails
- Application parts should be tested
- React framework
- Application state management using Redux or GraphQL client-side framework
- Pagination and styling is not necessary, table of exhange rates will be good enough
- Code should be pushed to remote GIT repository for review

# Installation

To get started building an application, clone the repository or download the source.
Then run `yarn install` (`npm install` will probably also work, but usage of yarn is recommended to make sure you get the right versions of all dependencies).

## NPM / Yarn scripts

The following scripts are provided to build, lint, etc:

* `clean` removes the build directory
* `build:dev` builds site for use in a development environment and outputs it to `dist`
* `build:prod` builds site for deployment to a production environment
* `start` starts the server for hot reloading
* `tslint` runs TSLint with the configured settings
* `test` runs all tests
* `coverage` runs all tests and displays test coverage

## Linting

This project comes with TSLint and a mostly unmodified preset for the [Airbnb style guide](https://github.com/airbnb/javascript/).

## Testing

Tests are written with Jest and picked up anywhere in the `src` directory if they include `.test` or `.spec` (e.g. `Home.spec.tsx`).
